handleFilter = (filter) => {
  const params = Object.entries(filter).reduce((acc, [key, value]) => {
    if (!value && value !== 0) return acc;
    // if (key === 'search') {
    //   acc.push({ name: { $regex: value, $options: 'i' } });
    // } else
    acc.push({ [key]: value });
    return acc;
  }, []);
  return params;
};

module.exports = handleFilter;
