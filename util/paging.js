handleLimitOffset = (_limit, _offset) => {
  var limit = _limit === -1 ? undefined : _limit + 1 || 11;
  var offset = _offset || 0;
  return { limit, offset };
};

handleNextPage = (data, limit, offset) => {
  var nextPage =
    limit && data.length === limit
      ? {
          offset: offset + limit - 1,
          limit: limit - 1,
        }
      : null;
  return nextPage;
};

module.exports = {
  handleLimitOffset,
  handleNextPage,
};
