respErr = ({ res, errorEnum, error }) => {
  return res.status(errorEnum.status || 200).send({
    code: errorEnum.code,
    message: errorEnum.message,
    debugMessage: errorEnum.debugMessage + (error ? '; Message:  ' + error : ''),
  });
};

module.exports = respErr;
