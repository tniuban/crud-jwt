removeEmpty = (obj) =>
  Object.keys(obj)
    .filter((k) => !!obj[k])
    .reduce((acc, k) => {
      acc[k] =
        typeof obj[k] === 'object' && !Array.isArray(obj[k])
          ? removeEmpty(obj[k])
          : obj[k];
      return acc;
    }, {});

module.exports = removeEmpty;
