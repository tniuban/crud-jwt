resp = ({ res, data, status = 200 }) => {
  return res.status(status).send({
    code: 0,
    data,
  });
};

module.exports = resp;
