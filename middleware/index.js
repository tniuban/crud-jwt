module.exports = {
  verifyToken: require('./verifyToken'),
  permissionRequired: require('./permissionRequired'),
};
