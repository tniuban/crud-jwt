const { ErrPermissionDenied } = require('../enum/error');
const { respErr } = require('../util');

permissionRequired = (_roleId) => (req, res, next) => {
  var userRoles = req.userRoles || [];
  if (userRoles.findIndex((id) => id === _roleId) === -1) {
    respErr({
      res,
      errorEnum: ErrPermissionDenied,
      error: "Require role '" + _roleId + "'",
    });
    return;
  }
  next();
};

module.exports = permissionRequired;
