const jwt = require('jsonwebtoken');
const config = require('../config/auth.config');
const { ErrUnauthenticated } = require('../enum/error');
const { respErr } = require('../util');

verifyToken = (req, res, next) => {
  let bearerToken = req.headers['authorization'];
  let token = bearerToken?.split(' ')?.[1];

  jwt.verify(token, config.secret, (error, decoded) => {
    if (error) {
      respErr({ res, errorEnum: ErrUnauthenticated, error: error.message });
      return;
    }
    req.userId = decoded.id;
    req.userRoles = decoded.roles;
    next();
  });
};

module.exports = verifyToken;
