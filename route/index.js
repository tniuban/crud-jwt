const categoryRoute = require('./category');
const brandRoute = require('./brand');
const productRoute = require('./product');
const authRoute = require('./auth');
const userRoute = require('./user');
const cartRoute = require('./cart');

module.exports = function (app) {
  app.use(function (_, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  app.use('/category', categoryRoute);
  app.use('/brand', brandRoute);
  app.use('/product', productRoute);
  app.use('/auth', authRoute);
  app.use('/user', userRoute);
  app.use('/cart', cartRoute);
};
