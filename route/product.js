const {
  createProduct,
  findProducts,
  findProductById,
  deleteProduct,
  updateProduct,
} = require('../controller/product');
const { verifyToken } = require('../middleware');

const router = require('express').Router();

router.post('/delete-product', [verifyToken], deleteProduct);
router.post('/find-product-by-id', [verifyToken], findProductById);
router.post('/create-product', [verifyToken], createProduct);
router.post('/update-product', [verifyToken], updateProduct);
router.post('/find-products', [verifyToken], findProducts);

module.exports = router;
