const {
  createCategory,
  findCategories,
  findCategoryById,
  deleteCategory,
  updateCategory,
} = require('../controller/category');
const { verifyToken } = require('../middleware');

const router = require('express').Router();

router.post('/delete-category', [verifyToken], deleteCategory);
router.post('/find-category-by-id', [verifyToken], findCategoryById);
router.post('/create-category', [verifyToken], createCategory);
router.post('/update-category', [verifyToken], updateCategory);
router.post('/find-categories', [verifyToken], findCategories);

module.exports = router;
