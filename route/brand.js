const {
  createBrand,
  findBrands,
  findBrandById,
  deleteBrand,
  updateBrand,
} = require('../controller/brand');
const { verifyToken, permissionRequired } = require('../middleware');

const router = require('express').Router();

router.post('/delete-brand', [verifyToken], deleteBrand);
router.post('/find-brand-by-id', [verifyToken], findBrandById);
router.post('/create-brand', [verifyToken], createBrand);
router.post('/update-brand', [verifyToken], updateBrand);
router.post('/find-brands', [verifyToken], findBrands);

module.exports = router;
