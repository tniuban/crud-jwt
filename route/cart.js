const {
  createCart,
  // findCarts,
  // findCartById,
  // deleteCart,
  // updateCart,
} = require('../controller/cart');
const { verifyToken, permissionRequired } = require('../middleware');

const router = require('express').Router();

router.post('/create-cart', [verifyToken], createCart);
// router.post('/delete-cart', [verifyToken], deleteCart);
// router.post('/find-cart-by-id', [verifyToken], findCartById);
// router.post('/update-cart', [verifyToken], updateCart);
// router.post('/find-carts', [verifyToken], findCarts);

module.exports = router;
