const {
  createUser,
  findUsers,
  findUserById,
  deleteUser,
  updateUser,
  changePassword,
} = require('../controller/user');
const { verifyToken, permissionRequired } = require('../middleware');

const router = require('express').Router();

router.post('/delete-user', [verifyToken], deleteUser);
router.post('/find-user-by-id', [verifyToken], findUserById);
router.post('/create-user', [verifyToken], createUser);
router.post('/update-user', [verifyToken], updateUser);
router.post('/find-users', [verifyToken], findUsers);
router.post('/change-password', [verifyToken], changePassword);

module.exports = router;
