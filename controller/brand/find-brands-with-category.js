findBrands = async (req, res) => {
    const { limit, offset } = paging(req.body.limit, req.body.offset);
    let brands;
    try {
      if (Array.isArray(req.body.categoryIds) && req.body.categoryIds.length) {
        brands = await Brand.find()
          .populate('categoryIds', 'name')
          .where('categoryIds')
          .in(req.body.categoryIds)
          .limit(limit)
          .skip(offset);
      } else
        brands = await Brand.find()
          .populate('categoryIds', 'name')
          .where('categoryIds')
          .in()
          .limit(limit)
          .skip(offset);
  
      const response = brands.map(({ _id: id, name, categoryIds: cIds }) => {
        const categoryIds = cIds.map((element) => ({
          id: element._id,
          name: element.name,
        }));
        return {
          id,
          name,
          categoryIds,
        };
      });
  
      resp({ res, data: response });
    } catch (error) {
      resp({ res, data: error.message });
    }
  };