const { ErrUnknown, ErrInvalidArgument } = require('../../enum/error');
const { Brand } = require('../../model');
const { resp, respErr } = require('../../util');

findBrandById = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Brand.ID is required',
    });
    return;
  }
  try {
    const brand = await Brand.findById(req.id).populate('categoryIds', 'name');
    const response = {
      id: brand._id,
      name: brand.name,
      categories: brand.categoryIds.map(({ _id: id, name }) => ({ id, name })),
    };
    resp({ res, data: response });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = findBrandById;
