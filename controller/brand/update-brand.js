const {
  ErrUnknown,
  ErrInvalidArgument,
  ErrBrandNotFound,
} = require('../../enum/error');
const { Brand, Category } = require('../../model');
const { respErr, resp } = require('../../util');
const removeEmpty = require('../../util/removeEmpty');

const updateBrand = async (req, res) => {
  const id = req.body.id;
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Brand.ID is required',
    });
    return;
  }
  const params = removeEmpty(req.body);
  try {
    const brand = new Brand(req.body);
    const oldBrand = await Brand.findByIdAndUpdate(id, { $set: params });

    if (!oldBrand || !oldBrand._id) {
      respErr({ res, errorEnum: ErrBrandNotFound });
      return;
    }

    // Remove categoryIds
    const rmFromCategoryIds = oldBrand.categoryIds.filter(
      (categoryId) => !brand.categoryIds.includes(categoryId)
    );
    if (rmFromCategoryIds.length) {
      const rm = await Category.updateMany(
        {
          _id: { $in: rmFromCategoryIds },
        },
        { $pull: { brandIds: oldBrand._id } }
      );
    }

    // Add categoryIds
    const addToCategoryIds = brand.categoryIds.filter(
      (categoryId) => !oldBrand.categoryIds.includes(categoryId)
    );
    if (addToCategoryIds.length) {
      await Category.updateMany(
        {
          _id: addToCategoryIds,
        },
        { $push: { brandIds: oldBrand._id } }
      );
    }

    resp({
      res,
      data: {
        id: oldBrand._id,
        name: oldBrand.name,
        categoryIds: brand.categoryIds,
        createdAt: oldBrand.createdAt,
        updatedAt: oldBrand.updatedAt,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = updateBrand;
