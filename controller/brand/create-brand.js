const { Brand, Category } = require('../../model');
const { resp, respErr } = require('../../util');
const { ErrBrandExisted, ErrInvalidArgument } = require('../../enum/error');
const createBrand = async (req, res) => {
  try {
    const brand = new Brand(req.body);
    const existingBrand = await Brand.findOne({ name: req.body.name });
    if (existingBrand) {
      respErr({ res, errorEnum: ErrBrandExisted });
      return;
    }
    const response = await brand.save();
    if (
      Array.isArray(req.body.categoryIds) &&
      req.body.categoryIds.length > 0
    ) {
      await Category.updateMany(
        {
          _id: req.body.categoryIds,
        },
        { $push: { brandIds: resp._id } }
      );
    }
    resp({
      res,
      data: {
        id: response._id,
        name: response.name,
        categoryIds: response.categoryIds,
        createdAt: response.createdAt,
        updatedAt: response.updatedAt,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrInvalidArgument, error: error.message });
  }
};

module.exports = createBrand;
