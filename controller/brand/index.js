const createBrand = require('./create-brand');
const findBrands = require('./find-brands');
const findBrandById = require('./find-brand-by-id');
const deleteBrand = require('./delete-brand');
const updateBrand = require('./update-brand');
const findBrandsWithCategory = require('./find-brands-with-category');

module.exports = {
  createBrand,
  findBrands,
  findBrandsWithCategory,
  findBrandById,
  deleteBrand,
  updateBrand,
};
