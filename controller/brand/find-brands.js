const { ErrUnknown } = require('../../enum/error');
const { Brand } = require('../../model');
const {
  resp,
  handleLimitOffset,
  handleNextPage,
  respErr,
} = require('../../util');

findBrands = async (req, res) => {
  const { limit, offset } = handleLimitOffset(req.body.limit, req.body.offset);
  const filter =
    Array.isArray(req.body.categoryIds) && req.body.categoryIds.length > 0
      ? {
          categoryIds: { $in: req.body.categoryIds || [] },
        }
      : {};

  try {
    const brands = await Brand.find(filter).limit(limit).skip(offset);

    const nextPage = handleNextPage(brands, limit, offset);
    if (nextPage) {
      brands.pop();
    }

    const response = brands.map(({ _id: id, name, categoryIds }) => ({
      id,
      name,
      categoryIds,
    }));

    resp({
      res,
      data: {
        brands: response,
        nextPage,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = findBrands;
