const {
  ErrUnknown,
  ErrInvalidArgument,
  ErrBrandNotFound,
} = require('../../enum/error');
const { Brand } = require('../../model');
const { resp, respErr } = require('../../util');

deleteBrand = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Messgage: Brand.ID is required',
    });
    return;
  }
  try {
    const brand = await Brand.findByIdAndRemove(req.body.id);
    if (!brand || !brand._id) {
      respErr({ res, errorEnum: ErrBrandNotFound });
      return;
    }
    resp({ res, data: brand });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = deleteBrand;
