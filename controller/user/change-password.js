var bcrypt = require('bcryptjs');
const { ErrUnknown, ErrWrongPassword } = require('../../enum/error');
const { User } = require('../../model');
const { respErr } = require('../../util');

changePassword = async (req, res) => {
  const userId = req.userId;
  const oldPassword = req.body.oldPassword;
  const newPasswrod = req.body.newPasswrod;
  try {
    const oldUser = await User.findById(userId);
    var passwordIsValid = bcrypt.compareSync(oldPassword, oldUser.password);
    if (!passwordIsValid) {
      respErr({
        res,
        errorEnum: ErrWrongPassword,
      });
      return;
    }
    oldUser.password = bcrypt.hashSync(newPasswrod, 8);
    await oldUser.save();
    resp({ res, data: {} });
  } catch (error) {
    respErr({
      res,
      errorEnum: ErrUnknown,
      error: error.message,
    });
  }
};

module.exports = changePassword;
