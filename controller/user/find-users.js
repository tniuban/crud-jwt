const { ErrUnknown } = require('../../enum/error');
const { User } = require('../../model');
const { respErr, resp, handleLimitOffset } = require('../../util');

findUsers = async (req, res) => {
  const { limit, offset } = handleLimitOffset(req.body.limit, req.body.offset);

  try {
    const users = await User.find().limit(limit).skip(offset);

    const nextPage = handleNextPage(users, limit, offset);
    if (nextPage) {
      users.pop();
    }
    const response = users.map(
      ({
        _id,
        firstName,
        lastName,
        maidenName,
        avatar,
        age,
        gender,
        emaill,
        phone,
        username,
        birthDate,
        address,
        roleIds,
        createdAt,
        updatedAt,
      }) => ({
        id: _id,
        firstName,
        lastName,
        maidenName,
        avatar,
        age,
        gender,
        emaill,
        phone,
        username,
        birthDate,
        address,
        roleIds,
        createdAt,
        updatedAt,
      })
    );
    resp({
      res,
      data: {
        users: response,
        nextPage,
      },
    });
  } catch (error) {
    respErr({
      res,
      errorEnum: ErrUnknown,
      error: error.message,
    });
  }
};

module.exports = findUsers;
