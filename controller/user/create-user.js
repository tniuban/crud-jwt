const { ErrUserExisted } = require('../../enum/error');
const { User } = require('../../model');
const { respErr, resp } = require('../../util');

const createUser = async (req, res) => {
  const findReq = [];

  if (req.body.username) {
    findReq.push({ username: req.body.username });
  }
  if (req.body.email) {
    findReq.push({ email: req.body.email });
  }

  try {
    const existingUser = await User.findOne({ $or: findReq });
    if (existingUser) {
      respErr({ res, errorEnum: ErrUserExisted });
      return;
    }
    const user = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      maidenName: req.maidenName,
      age: req.body.age,
      gender: req.body.gender,
      emaill: req.body.emaill,
      phone: req.body.phone,
      username: req.body.username,
      password: bcrypt.hashSync(req.body.password, 8),
      birthDate: req.body.birthDate,
      address: {
        address: req.body.address?.address,
        city: req.body.address?.city,
        postalCode: req.body.address?.postalCode,
        state: req.body.address?.state,
      },
      roleIds: req.body.roleIds,
    });

    const response = await user.save();
    resp({ res, data: response });
  } catch (error) {
    respErr({ res, errorEnum: ErrInvalidArgument, error: error.message });
  }
};

module.exports = createUser;
