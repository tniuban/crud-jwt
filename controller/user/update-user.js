const { ErrUserExisted } = require('../../enum/error');
const { User } = require('../../model');
const { respErr, resp, removeEmpty } = require('../../util');

const updateUser = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'User.ID is required',
    });
    return;
  }

  const existingUserFilter = [];
  if (req.body.username) {
    existingUserFilter.push({ username: req.body.username });
  }
  if (req.body.email) {
    existingUserFilter.push({ email: req.body.email });
  }

  try {
    const existingUser = await User.findOne(existingUserFilter);
    if (existingUser) {
      respErr({ res, errorEnum: ErrUserExisted });
      return;
    }
    const user = removeEmpty({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      maidenName: req.maidenName,
      age: req.body.age,
      gender: req.body.gender,
      emaill: req.body.emaill,
      phone: req.body.phone,
      username: req.body.username,
      password: req.body.password
        ? bcrypt.hashSync(req.body.password, 8)
        : null,
      birthDate: req.body.birthDate,
      address: {
        address: req.body.address?.address,
        city: req.body.address?.city,
        postalCode: req.body.address?.postalCode,
        state: req.body.address?.state,
      },
      roleIds: req.body.roleIds,
    });

    const response = await User.findByIdAndUpdate(id, { $set: user });
    resp({ res, data: { response, user } });
  } catch (error) {
    respErr({ res, errorEnum: ErrInvalidArgument, error: error.message });
  }
};

module.exports = updateUser;
