const {
  ErrUserNotFound,
  ErrUnknown,
  ErrInvalidArgument,
} = require('../../enum/error');
const { User } = require('../../model');
const { respErr, resp } = require('../../util');

findUserById = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'User.ID is required',
    });
    return;
  }
  try {
    const user = await User.findById(req.body.id);
    if (!user) {
      respErr({ res, errorEnum: ErrUserNotFound });
      return;
    }
    resp({
      res,
      data: {
        id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        maidenName: user.maidenName,
        avatar: user.avatar,
        age: user.age,
        gender: user.gender,
        emaill: user.emaill,
        phone: user.phone,
        username: user.username,
        birthDate: user.birthDate,
        address: user.address,
        roleIds: user.roleIds,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = findUserById;
