var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const config = require('../../config/auth.config');
const { User } = require('../../model');
const { respErr, resp } = require('../../util');
const {
  ErrUnknown,
  ErrWrongUsername,
  ErrWrongPassword,
} = require('../../enum/error');

login = async (req, res) => {
  try {
    const user = await User.findOne({
      $or: [{ email: req.body.username }, { username: req.body.username }],
    });
    if (!user) {
      respErr({ res, errorEnum: ErrWrongUsername, error: error.message });
      return;
    }
    var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

    if (!passwordIsValid) {
      respErr({ res, errorEnum: ErrWrongPassword });
      return;
    }
    var roles = user.roles || [];
    var token = jwt.sign({ id: user.id, roles }, config.secret, {
      expiresIn: 86400, // 24 hours
    });

    var refreshToken = jwt.sign({ id: user.id, roles }, config.secret, {
      expiresIn: 86400 * 30, // 24 hours
    });
    resp({
      res,
      data: {
        id: user._id,
        profile: {
          firstName: user.firstName,
          lastName: user.lastName,
          maidenName: user.maidenName,
          avatar: user.avatar,
          age: user.age,
          gender: user.gender,
          email: user.email,
          phone: user.phone,
          birthDate: user.birthDate,
          address: {
            address: user.address?.address,
            city: user.address?.city,
            postalCode: user.address?.postalCode,
            state: user.address?.state,
          },
        },
        roles,
        token,
        refreshToken,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = login;
