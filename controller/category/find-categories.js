const { ErrUnknown } = require('../../enum/error');
const { Category } = require('../../model');
const {
  handleLimitOffset,
  respErr,
  resp,
  handleNextPage,
} = require('../../util');

findCategories = async (req, res) => {
  const { limit, offset } = handleLimitOffset(req.body.limit, req.body.offset);
  try {
    const categories = await Category.find()
      //   .populate('brandIds', 'name')
      .limit(limit)
      .skip(offset);

    const nextPage = handleNextPage(categories, limit, offset);

    if (nextPage) {
      categories.pop();
    }

    const response = categories.map(({ _id: id, name, brandIds }) => ({
      id,
      name,
      brandIds,
    }));

    resp({
      res,
      data: {
        categories: response,
        nextPage,
      },
    });
  } catch (error) {
    respErr({
      res,
      errorEnum: ErrUnknown,
      error: error.message,
    });
  }
};

module.exports = findCategories;
