const createCategory = require('./create-category');
const findCategories = require('./find-categories');
const findCategoryById = require('./find-category-by-id');
const deleteCategory = require('./delete-category');

module.exports = {
  createCategory,
  findCategories,
  findCategoryById,
  deleteCategory,
  updateCategory: deleteCategory,
};
