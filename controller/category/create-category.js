const { ErrCategoryExisted } = require('../../enum/error');
const { Category, Brand } = require('../../model');
const { respErr, resp } = require('../../util');

const createCategory = async (req, res) => {
  try {
    const existingCategory = await Category.findOne({ name: req.body.name });
    if (existingCategory) {
      respErr({ res, errorEnum: ErrCategoryExisted });
      return;
    }
    const category = new Category(req.body);
    const response = await category.save();
    if (Array.isArray(req.body.brandIds) && req.body.brandIds.length > 0) {
      const a = await Brand.updateMany(
        {
          _id: req.body.brandIds,
        },
        { $push: { categoryIds: resp._id } }
      );
    }
    resp({
      res,
      data: {
        id: response._id,
        name: response.name,
        brandIds: response.brandIds,
        createdAt: response.createdAt,
        updatedAt: response.updatedAt,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrInvalidArgument, error: error.message });
  }
};

module.exports = createCategory;
