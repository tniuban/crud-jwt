const { ErrUnknown, ErrInvalidArgument } = require('../../enum/error');
const { Category } = require('../../model');
const { resp, respErr } = require('../../util');

findCategoryById = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Category.ID is required',
    });
    return;
  }
  try {
    const category = await Category.findById(req.body.id).populate(
      'brandIds',
      'name'
    );
    resp({ res, data: category });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = findCategoryById;
