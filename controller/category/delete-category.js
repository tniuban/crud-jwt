const { ErrUnknown, ErrInvalidArgument } = require('../../enum/error');
const { Category } = require('../../model');
const { resp, respErr } = require('../../util');

deleteCategory = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Category.ID is required',
    });
    return;
  }
  try {
    await Category.findByIdAndRemove(req.body.id);
    resp({ res, data: {} });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = deleteCategory;
