const { ErrInvalidArgument, ErrProductNotFound } = require('../../enum/error');
const { Product, Cart } = require('../../model');
const { respErr, resp } = require('../../util');

const createProduct = async (req, res) => {
  try {
    const userId = req.userId;
    const items = req.body.items;
    if (!Array.isArray(items) || items.length === 0) {
      respErr({
        res,
        errorEnum: ErrInvalidArgument,
        error: 'Cart.Items must be non-empty',
      });
      return;
    }
    const products = await Product.find({
      _id: { $in: items.map(({ id }) => id) },
    });
    if (products.length !== items.length) {
      respErr({
        res,
        errorEnum: ErrProductNotFound,
        error: error.message,
      });
      return;
    }
    const cart = new Cart({
      userId,
      items,
    });

    cart.items.forEach(async (item) => {
      await Product.updateOne({ _id: item._id }, { $inc: { stock: -1 } });
    });

    const response = await cart.save();
    resp({
      res,
      data: {
        id: response._id,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrInvalidArgument, error: error.message });
  }
};

module.exports = createProduct;
