const createProduct = require('./create-product');
const findProducts = require('./find-products');
const findProductById = require('./find-product-by-id');
const deleteProduct = require('./delete-product');
const updateProduct = require('./update-product');

module.exports = {
  createProduct,
  findProducts,
  findProductById,
  deleteProduct,
  updateProduct,
};
