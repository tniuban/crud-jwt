const { ErrProductNotFound, ErrUnknown, ErrInvalidArgument } = require('../../enum/error');
const { Product } = require('../../model');
const { respErr, resp } = require('../../util');

findProductById = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Product.ID is required',
    });
    return;
  }
  try {
    const product = await Product.findById(req.body.id).populate(
      'brandIds',
      'name'
    );
    if (!product) {
      respErr({ res, errorEnum: ErrProductNotFound });
      return;
    }
    resp({ res, data: product });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = findProductById;
