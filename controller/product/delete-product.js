const { ErrUnknown, ErrInvalidArgument } = require('../../enum/error');
const { Product } = require('../../model');
const { respErr } = require('../../util');

deleteProduct = async (req, res) => {
  if (!req.body.id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Product.ID is required',
    });
    return;
  }
  try {
    await Product.findByIdAndRemove(req.body.id);
    resp({ res, data: {} });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = deleteProduct;
