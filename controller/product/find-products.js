const { ErrUnknown } = require('../../enum/error');
const { Product } = require('../../model');
const {
  respErr,
  resp,
  handleLimitOffset,
  handleFilter,
} = require('../../util');

findProducts = async (req, res) => {
  const { limit, offset } = handleLimitOffset(req.body.limit, req.body.offset);

  const filter = handleFilter({
    categoryId: req.body.categoryId,
    brandId: req.body.brandId,
    name: req.body.name,
  });
  console.log('filter', filter);
  try {
    const products = await Product.find(...filter)
      .populate('brandId', 'name')
      .populate('categoryId', 'name')
      .limit(limit)
      .skip(offset);

    const nextPage = handleNextPage(products, limit, offset);
    if (nextPage) {
      products.pop();
    }
    const response = products.map(
      ({
        _id,
        name,
        brandId,
        stock,
        price,
        discountPrice,
        thumbnail,
        rate,
        totalComment,
        giftPrice,
        details,
        categoryId,
        createdAt,
        updatedAt,
      }) => ({
        id: _id,
        name,
        brandId: brandId._id,
        brandName: brandId.name,
        stock,
        price,
        discountPrice,
        thumbnail,
        rate,
        totalComment,
        giftPrice,
        details,
        categoryId: categoryId._id,
        categoryName: categoryId.name,
        createdAt,
        updatedAt,
      })
    );
    resp({
      res,
      data: {
        products: response,
        nextPage,
      },
    });
  } catch (error) {
    respErr({
      res,
      errorEnum: ErrUnknown,
      error: error.message,
    });
  }
};

module.exports = findProducts;
