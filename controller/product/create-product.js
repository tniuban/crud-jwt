const {
  ErrProductExisted,
  ErrCategoryNotFound,
  ErrBrandNotFound,
} = require('../../enum/error');
const { Product, Category } = require('../../model');
const { respErr } = require('../../util');

const createProduct = async (req, res) => {
  try {
    const existingProduct = await Product.findOne({ name: req.body.name });
    if (existingProduct) {
      respErr({ res, errorEnum: ErrProductExisted });
      return;
    }
    const product = new Product(req.body);

    // Check category
    const category = await Category.findById(req.body.categoryId);
    console.log('product.categoryId', product.categoryId);
    if (!category || !category._id) {
      respErr({ res, errorEnum: ErrCategoryNotFound });
      return;
    }

    // Check brand
    if (!category.brandIds.includes(product.brandId)) {
      respErr({ res, errorEnum: ErrBrandNotFound });
      return;
    }

    const response = await product.save();
    resp({ res, data: response });
  } catch (error) {
    respErr({ res, errorEnum: ErrInvalidArgument, error: error.message });
  }
};

module.exports = createProduct;
