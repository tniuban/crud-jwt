const {
  ErrUnknown,
  ErrInvalidArgument,
  ErrProductExisted,
} = require('../../enum/error');
const { Product } = require('../../model');
const { respErr, removeEmpty } = require('../../util');

deleteProduct = async (req, res) => {
  var id = req.body.id;
  if (!id) {
    respErr({
      res,
      errorEnum: ErrInvalidArgument,
      error: 'Product.ID is required',
    });
    return;
  }

  const params = removeEmpty(req.body);

  try {
    var name = req.body.name;
    if (name) {
      const existingProductName = await Product.findOne({ name });
      if (!existingProductName) {
        respErr({ res, errorEnum: ErrProductExisted });
        return;
      }
    }

    // update product
    const oldProduct = await Product.findByIdAndUpdate(id, { $set: params });
    resp({
      res,
      data: {
        id: oldProduct._id,
      },
    });
  } catch (error) {
    respErr({ res, errorEnum: ErrUnknown, error: error.message });
  }
};

module.exports = deleteProduct;
