const mongoose = require('mongoose');

const Cart = mongoose.model(
  'Cart',
  new mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
      items: [
        {
          id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product',
          },
          price: {
            type: Number,
            required: true,
          },
          quantity: {
            type: Number,
            required: true,
          },
        },
      ],
    },
    { timestamps: true }
  )
);

module.exports = Cart;
