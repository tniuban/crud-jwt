const mongoose = require('mongoose');

const Role = mongoose.model(
  'Role',
  new mongoose.Schema(
    {
      id: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      permissions: {
        type: [String],
        required: true,
      },
    },
    { timestamps: true }
  )
);

module.exports = Role;
