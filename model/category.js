const mongoose = require('mongoose');

const Category = mongoose.model(
  'Category',
  new mongoose.Schema(
    {
      name: {
        type: String,
        required: true,
      },
      brandIds: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Brand',
        },
      ],
    },
    { timestamps: true }
  )
);

module.exports = Category;