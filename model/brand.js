const mongoose = require('mongoose');

const Brand = mongoose.model(
  'Brand',
  new mongoose.Schema(
    {
      name: {
        type: String,
        required: true,
      },
      categoryIds: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Category',
        },
      ],
    },
    { timestamps: true }
  )
);

module.exports = Brand;
