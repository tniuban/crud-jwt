const mongoose = require('mongoose');

const User = mongoose.model(
  'User',
  new mongoose.Schema(
    {
      firstName: {
        type: String,
        require: true,
      },
      lastName: {
        type: String,
        require: true,
      },
      maidenName: String,
      avatar: String,
      age: {
        type: Number,
        require: true,
      },
      gender: {
        type: String,
        require: true,
      },
      emaill: {
        type: String,
        require: true,
      },
      phone: {
        type: String,
        require: true,
      },
      username: {
        type: String,
        require: true,
      },
      password: {
        type: String,
        require: true,
      },
      birthDate: {
        type: String,
        require: true,
      },
      address: {
        address: String,
        city: String,
        postalCode: String,
        state: String,
      },
      roleIds: [
        {
          type: mongoose.Schema.Types.String,
          ref: 'Role',
        },
      ],
    },
    { timestamps: true }
  )
);

module.exports = User;
