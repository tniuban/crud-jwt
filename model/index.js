const User = require('./user.js');
const Role = require('./role.js');
const Category = require('./category.js');
const Brand = require('./brand.js');
const Product = require('./product.js');
const Cart = require('./cart.js');

module.exports = {
  Role,
  Brand,
  Category,
  User,
  Product,
  Cart,
};
