const mongoose = require('mongoose');

const Product = mongoose.model(
  'Product',
  new mongoose.Schema(
    {
      name: {
        type: String,
        required: true,
      },
      brandId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Brand',
        required: true,
      },
      stock: {
        type: Number,
        required: true,
      },
      price: {
        type: Number,
        required: true,
      },
      discountPrice: {
        type: Number,
        required: true,
        default: 0,
      },
      thumbnail: {
        type: String,
        required: true,
      },
      rate: {
        type: Number,
        required: true,
      },
      totalComment: {
        type: Number,
        default: 0,
      },
      giftPrice: {
        type: Number,
        default: 0,
      },
      details: {
        type: [String],
        required: true,
      },
      categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
      },
    },
    { timestamps: true }
  )
);

module.exports = Product;
