const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
var bodyParser = require('body-parser');

const app = express();
const dbConfig = require('./config/db.config');

var corsOptions = {
  origin: 'http://localhost:8080',
};

// dotenv.config();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('common'));

mongoose
  .connect(dbConfig.dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Successfully connect to MongoDB.');
  })
  .catch((err) => {
    console.error('Connection error:', err);
    process.exit();
  });

// set port, listen for requests
app.listen(dbConfig.port, () => {
  console.log(`Server is running on port ${dbConfig.port}.`);
});

require('./route')(app);
