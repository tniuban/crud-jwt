errType = {
  1001: 'ErrCategoryExisted',
  1002: 'ErrCategoryNotFound',
  1003: 'ErrCategoryNotOwnBrand',
  1011: 'ErrBrandExisted',
  1012: 'ErrBrandNotFound',
  1021: 'ErrProductExisted',
  1022: 'ErrProductNotFound',
  1031: 'ErrUserExisted',
  1032: 'ErrUserNotFound',
  2000: 'ErrUnknown',
  2001: 'ErrUnauthenticated',
  2002: 'ErrInvalidArgument',
  2003: 'ErrWrongUsername',
  2004: 'ErrWrongPassword',
  2003: 'ErrPermissionDenied',

  //   1001: 'ErrInvalidArgument',
  //   1002: 'ErrWrongEmail',
  //   1003: 'ErrWrongPassword',
  //   1004: 'ErrAccountInActive',
  //   1005: 'ErrUserNotFound',
  //   1006: 'ErrPermissionDenied',
  //   1007: 'ErrAccountExisted',
  //   1008: 'ErrInvalidOldPassword',
  //   1009: 'ErrInvalidOTP',
  //   1010: 'ErrAzureDeviceOffline',
  //   1011: 'ErrAzureDeviceDeviceNotFound',
  //   1012: 'ErrCanNotEditFarmImportWarehouse',
  //   1013: 'ErrCanNotDeleteFarmImportWarehouse',
  //   1014: 'ErrFarmImportWarehouseAlreadyApproved',
  //   1015: 'ErrCanNotEditModuleTransferWarehouse',
  //   1016: 'ErrCanNotDeleteModuleTransferWarehouse',
  //   1017: 'ErrModuleTransferWarehouseAlreadyApproved',
  //   1018: 'ErrShrimpDiaryNotFound',
  //   1019: 'ErrNotEnoughtProductQuantityToUse',
  //   1020: 'ErrNotFoundModuleWarehouse',
  //   1021: 'ErrRoleNotFound',
  //   1022: 'ErrUserHasExistedRole',
  //   1023: 'ErrInvalidDateRange',
  //   1024: 'ErrNotFoundReportPerformance',
  //   1025: 'ErrNotFoundActiveSeason',
  //   1026: 'ErrIotPondNotFound',
  //   1027: 'ErrIotPondAssignExisted',
  //   1028: 'ErrPondAssignExisted',
  //   1029: 'ErrTaskCanNotModify',
  //   1030: 'ErrEdgeDeviceNotFound',
  //   4031: 'ErrCategoryExisted',
  //   4032: 'ErrCategoryNotFound',
  //   1033: 'ErrZoneCodeExisted',
  //   1034: 'ErrModuleCodeExisted',
  //   1035: 'ErrProductCodeExisted',
  //   1036: 'ErrNotFoundShrimpDiaryActiveOrHarvesting',
  //   1038: 'ErrMovingToPondNotFinishDiaryYet',
  //   1039: 'ErrFarmNotFound',
  //   1040: 'ErrProductFarmAlreadyExtis',
  //   4041: 'ErrBrandExisted',
  //   4042: 'ErrBrandNotFound',
  //   4043: 'ErrProductRequireCategoryAndBrand',
  //   1044: 'ErrUserAlreadyOwnFarm',
  //   1045: 'ErrUserAlreadyOwnZone',
  //   1046: 'ErrUserAlreadyOwnModule',
  //   1047: 'ErrUserAlreadyOwnPond',
  //   1048: 'ErrCanNotEditProductCategory',
  //   1049: 'ErrNotShrimpInformationSourcePondYet',
  //   1050: 'ErrTimeZoneInvalid',
  //   4051: 'ErrProductExisted',
  //   4052: 'ErrProductNotFound',
  //   4053: 'ErrProductRequireCategoryAndBrand',
  //   1054: 'ErrUserNotOwnModule',
  //   1055: 'ErrUserNotOwnPond',
  //   1056: 'ErrNotShrimpInformationTargetPondYet',
  //   2001: 'ErrUnauthenticated',
  //   3001: 'ErrAzureSqliteIsCorupted',
  //   3002: 'ErrAzurePondIsNotExisted',
  //   3003: 'ErrAzureDateDontMatch',
  //   3004: 'ErrAzureControlModeIsNotSupported',
  //   3005: 'ErrAzureDeviceNotOke',
  //   3006: 'ErrAzureInvalidInpuFormat',
  //   3007: 'ErrAzureDeviceIsNotExisted',
  //   3008: 'ErrAzureIotDeviceIsExisted',
  //   3009: 'ErrAzureDeviceTypeInvalid',
  //   3010: 'ErrAzureMotiveBoxIsExisted',
  //   3011: 'ErrAzurePondExisted',
  //   3012: 'ErrAzureMotiveBoxNotExisted',
  //   3013: 'ErrAzureEmptyData',
  //   3014: 'ErrAzurePondTypeInvalid',
  //   3015: 'ErrAzureTableNotExisted',
  //   3016: 'ErrAzureFeedingScheduleDataInvalid',
  //   3017: 'ErrAzureIotFeedingSpeedInvalid',
  //   3018: 'ErrAzureModeTypeInvalid',
  //   3019: 'ErrAzureQuantityInvalid',
  //   3020: 'ErrAzureAddressInvalid',
  //   3021: 'ErrAzureAddressTaken',
  //   3022: 'ErrAzureHarvestingDayNotExisted',
  //   3023: 'ErrAzureUnitIDSetup',
  //   3024: 'ErrAzureUnitIDExisted',
  //   3025: 'ErrAzureFcExisted',
  //   3026: 'ErrAzureFcIsNotExisted',
  //   3027: 'ErrAzureSensorExisted',
  //   3028: 'ErrAzureSensorIsNotExisted',
  //   3029: 'ErrAzureImportFaild',
  //   3030: 'ErrAzureValueIsDuplicateAndHaveEvent',
  //   3031: 'ErrAzureValueIsDuplicateAndDontHaveEvent',
  //   3032: 'ErrAzureTrackingTypeIsNotExited',
  //   3033: 'ErrAzureCannotControlFeeder',
  //   3034: 'ErrAzureTheDrainageHoleIsAlmostEmpty',
  //   3401: 'ErrAzureMissingParams',
};

const errMsg = {
  1001: 'Category already existed',
  1002: 'Category not found',
  1003: 'Category not own brand',
  1011: 'Brand already existed',
  1012: 'Brand not found',
  1021: 'Product already existed',
  1022: 'Product not found',
  1021: 'User already existed',
  1022: 'User not found',
  2000: 'Something went wrong',
  2001: 'Unauthenticated',
  2002: 'Invalid argument',
  2003: 'Wrong username',
  2004: 'Wrong password',
  2005: 'Permission denied',
  //   1001: 'Invalid argument',
  //   1002: 'Wrong email',
  //   1003: 'Wrong password',
  //   1004: 'Your account is inactive',
  //   1005: 'User not found',
  //   1006: 'Permission denied',
  //   1007: 'Account already existed',
  //   1008: 'Old password does not match',
  //   1009: 'Invalid OTP',
  //   1010: 'Azure device is offline',
  //   1011: 'Azure device not found',
  //   1012: 'Only import status &#39;DRAFT&#39; can edit',
  //   1013: 'Only import status &#39;DRAFT&#39; can delete',
  //   1014: 'Farm import already approved',
  //   1015: 'Only transfer status &#39;DRAFT&#39; can edit',
  //   1016: 'Only transfer status &#39;DRAFT&#39; can delete',
  //   1017: 'Module transfer already approved',
  //   1018: 'Shrimp Diary Not Found',
  //   1019: 'Not Enought Product Quantity',
  //   1020: 'Not Found Module Warehouse',
  //   1021: 'Role not found',
  //   1022: 'User has existed role',
  //   1023: 'Invalid date range',
  //   1024: 'Report performance not found',
  //   1025: 'Not found active season',
  //   1026: 'Iot pond not found',
  //   1027: 'Iot pond assign existed',
  //   1028: 'Pond assign existed',
  //   1029: 'Task can not modify',
  //   1030: 'Edge device not found',
  //   4031: 'Category already existed',
  //   4032: 'Category not found',
  //   1033: 'Zone code Existed',
  //   1034: 'Zone code Existed',
  //   1035: 'Product code Existed',
  //   1036: 'Not Found Shrimp Diary Active Or Harvesting',
  //   1038: 'Pond not finish diary yet',
  //   1039: 'Farm Not Found',
  //   1040: 'Product farm already exist',
  //   4041: 'Brand already existed',
  //   4042: 'Brand not found',
  //   1043: 'Product Exits Warehouse',
  //   1044: 'User Already Own Farm',
  //   1045: 'User Already Own Zone',
  //   1046: 'User Already Own Module',
  //   1047: 'User Already Own Pond',
  //   1048: 'Product already used. Can not edit category',
  //   1049: 'Not Shrimp Information Source Pond Yet',
  //   1050: 'Timezone in valid',
  //   4051: 'Product already existed',
  //   4052: 'Product not found',
  //   4053: 'Product must have category and brand',
  //   1054: 'User Not Own Module',
  //   1055: 'User Not Own Pond',
  //   1056: 'Not Shrimp Information Target Pond Yet',
  //   2001: 'Unauthenticated',
  //   3001: 'Could not process command because sqlite is corupted',
  //   3002: 'Pond is not existed',
  //   3003: 'Date doesn&#39;t match',
  //   3004: 'Control mode is not supported',
  //   3005: 'Device is not OK for controlling',
  //   3006: 'Invalid input format',
  //   3007: 'Device is not existed',
  //   3008: 'Device is existed',
  //   3009: 'Type of device is invalid',
  //   3010: 'MotiveBox is existed',
  //   3011: 'Pond is existed',
  //   3012: 'MotiveBox is not existed',
  //   3013: 'Cannot update data without parameter',
  //   3014: 'Type of pond is invalid',
  //   3015: 'Table is not existed',
  //   3016: 'Data of feeding schedule is invalid',
  //   3017: 'Data of feeding speed is invalid',
  //   3018: 'Mode type is invalid',
  //   3019: 'Quantity is invalid',
  //   3020: 'Address is invalid',
  //   3021: 'Address is taken',
  //   3022: 'Harvesting day is not existed',
  //   3023: 'UnitId setup error',
  //   3024: 'UnitId is existed',
  //   3025: 'Fc is existed',
  //   3026: 'Fc is not existed',
  //   3027: 'Sensor is existed',
  //   3028: 'Sensor is not existed',
  //   3029: 'Import failed',
  //   3030: 'Value is duplicated and have event',
  //   3031: 'Value is duplicated and dont have event',
  //   3032: 'Tracking type is not exited',
  //   3033: 'Cannot control Feeder',
  //   3034: 'The drainage hole is almost empty',
  //   3401: 'Missing parameter for this process',
};

errStatus = {
  2001: 401,
};

function ErrorResponse(code) {
  return {
    code: code,
    status: errStatus[code] || 200,
    message: errMsg[code],
    debugMessage: 'Code: ' + errType[code],
  };
}
module.exports = {
  ErrCategoryExisted: ErrorResponse(1001),
  ErrCategoryNotFound: ErrorResponse(1002),
  ErrCategoryNotOwnBrand: ErrorResponse(1003),
  ErrBrandExisted: ErrorResponse(1011),
  ErrBrandNotFound: ErrorResponse(1012),
  ErrProductExisted: ErrorResponse(1021),
  ErrProductNotFound: ErrorResponse(1022),
  ErrUnknown: ErrorResponse(2000),
  ErrUnauthenticated: ErrorResponse(2001),
  ErrInvalidArgument: ErrorResponse(2002),
  ErrWrongUsername: ErrorResponse(2003),
  ErrWrongPassword: ErrorResponse(2004),
  ErrPermissionDenied: ErrorResponse(2005),
  // ErrUnknown: ErrorResponse(1000),
  // ErrInvalidArgument: ErrorResponse(1001),
  // ErrWrongEmail: ErrorResponse(1002),
  // ErrWrongPassword: ErrorResponse(1003),
  // ErrAccountInActive: ErrorResponse(1004),
  // ErrUserNotFound: ErrorResponse(1005),
  // ErrPermissionDenied: ErrorResponse(1006),
  // ErrAccountExisted: ErrorResponse(1007),
  // ErrInvalidOldPassword: ErrorResponse(1008),
  // ErrInvalidOTP: ErrorResponse(1009),
  // ErrAzureDeviceOffline: ErrorResponse(1010),
  // ErrAzureDeviceDeviceNotFound: ErrorResponse(1011),
  // ErrCanNotEditFarmImportWarehouse: ErrorResponse(1012),
  // ErrCanNotDeleteFarmImportWarehouse: ErrorResponse(1013),
  // ErrFarmImportWarehouseAlreadyApproved: ErrorResponse(1014),
  // ErrCanNotEditModuleTransferWarehouse: ErrorResponse(1015),
  // ErrCanNotDeleteModuleTransferWarehouse: ErrorResponse(1016),
  // ErrModuleTransferWarehouseAlreadyApproved: ErrorResponse(1017),
  // ErrShrimpDiaryNotFound: ErrorResponse(1018),
  // ErrNotEnoughtProductQuantityToUse: ErrorResponse(1019),
  // ErrNotFoundModuleWarehouse: ErrorResponse(1020),
  // ErrRoleNotFound: ErrorResponse(1021),
  // ErrUserHasExistedRole: ErrorResponse(1022),
  // ErrInvalidDateRange: ErrorResponse(1023),
  // ErrNotFoundReportPerformance: ErrorResponse(1024),
  // ErrNotFoundActiveSeason: ErrorResponse(1025),
  // ErrIotPondNotFound: ErrorResponse(1026),
  // ErrIotPondAssignExisted: ErrorResponse(1027),
  // ErrPondAssignExisted: ErrorResponse(1028),
  // ErrTaskCanNotModify: ErrorResponse(1029),
  // ErrEdgeDeviceNotFound: ErrorResponse(1030),
  // ErrCategoryExisted: ErrorResponse(4031),
  // ErrCategoryNotFound: ErrorResponse(4032),
  // ErrZoneCodeExisted: ErrorResponse(1033),
  // ErrModuleCodeExisted: ErrorResponse(1034),
  // ErrProductCodeExisted: ErrorResponse(1035),
  // ErrNotFoundShrimpDiaryActiveOrHarvesting: ErrorResponse(1036),
  // ErrMovingToPondNotFinishDiaryYet: ErrorResponse(1038),
  // ErrFarmNotFound: ErrorResponse(1039),
  // ErrProductFarmAlreadyExtis: ErrorResponse(1040),
  // ErrBrandExisted: ErrorResponse(4041),
  // ErrBrandNotFound: ErrorResponse(4042),
  // ErrProductExitsWarehouse: ErrorResponse(1043),
  // ErrUserAlreadyOwnFarm: ErrorResponse(1044),
  // ErrUserAlreadyOwnZone: ErrorResponse(1045),
  // ErrUserAlreadyOwnModule: ErrorResponse(1046),
  // ErrUserAlreadyOwnPond: ErrorResponse(1047),
  // ErrCanNotEditProductCategory: ErrorResponse(1048),
  // ErrNotShrimpInformationSourcePondYet: ErrorResponse(1049),
  // ErrTimeZoneInvalid: ErrorResponse(1050),
  // ErrProductExisted: ErrorResponse(4051),
  // ErrProductNotFound: ErrorResponse(4052),
  // ErrProductRequireCategoryAndBrand: ErrorResponse(4053),
  // ErrUserNotOwnModule: ErrorResponse(1054),
  // ErrUserNotOwnPond: ErrorResponse(1055),
  // ErrNotShrimpInformationTargetPondYet: ErrorResponse(1056),
  // ErrUnauthenticated: ErrorResponse(2001),
  // ErrAzureSqliteIsCorupted: ErrorResponse(3001),
  // ErrAzurePondIsNotExisted: ErrorResponse(3002),
  // ErrAzureDateDontMatch: ErrorResponse(3003),
  // ErrAzureControlModeIsNotSupported: ErrorResponse(3004),
  // ErrAzureDeviceNotOke: ErrorResponse(3005),
  // ErrAzureInvalidInpuFormat: ErrorResponse(3006),
  // ErrAzureDeviceIsNotExisted: ErrorResponse(3007),
  // ErrAzureIotDeviceIsExisted: ErrorResponse(3008),
  // ErrAzureDeviceTypeInvalid: ErrorResponse(3009),
  // ErrAzureMotiveBoxIsExisted: ErrorResponse(3010),
  // ErrAzurePondExisted: ErrorResponse(3011),
  // ErrAzureMotiveBoxNotExisted: ErrorResponse(3012),
  // ErrAzureEmptyData: ErrorResponse(3013),
  // ErrAzurePondTypeInvalid: ErrorResponse(3014),
  // ErrAzureTableNotExisted: ErrorResponse(3015),
  // ErrAzureFeedingScheduleDataInvalid: ErrorResponse(3016),
  // ErrAzureIotFeedingSpeedInvalid: ErrorResponse(3017),
  // ErrAzureModeTypeInvalid: ErrorResponse(3018),
  // ErrAzureQuantityInvalid: ErrorResponse(3019),
  // ErrAzureAddressInvalid: ErrorResponse(3020),
  // ErrAzureAddressTaken: ErrorResponse(3021),
  // ErrAzureHarvestingDayNotExisted: ErrorResponse(3022),
  // ErrAzureUnitIDSetup: ErrorResponse(3023),
  // ErrAzureUnitIDExisted: ErrorResponse(3024),
  // ErrAzureFcExisted: ErrorResponse(3025),
  // ErrAzureFcIsNotExisted: ErrorResponse(3026),
  // ErrAzureSensorExisted: ErrorResponse(3027),
  // ErrAzureSensorIsNotExisted: ErrorResponse(3028),
  // ErrAzureImportFaild: ErrorResponse(3029),
  // ErrAzureValueIsDuplicateAndHaveEvent: ErrorResponse(3030),
  // ErrAzureValueIsDuplicateAndDontHaveEvent: ErrorResponse(3031),
  // ErrAzureTrackingTypeIsNotExited: ErrorResponse(3032),
  // ErrAzureCannotControlFeeder: ErrorResponse(3033),
  // ErrAzureTheDrainageHoleIsAlmostEmpty: ErrorResponse(3034),
  // ErrAzureMissingParams: ErrorResponse(3401),
};
